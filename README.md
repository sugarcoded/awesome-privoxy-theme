# Awesome Privoxy Theme

My awesome custom Privoxy theme.

## Getting started/Installation
Install Privoxy and in the main config set it to use a custom templates dir.  
Copy these files to your custom dir.  
Boom stonks

## Authors and acknowledgment
Thanks to Ian, Tim and Lee for figuring out how to access custom images in Privoxy templates that were stored locally.  
(They need to be put in /usr/share/doc/privoxy/user-manual/ and then called by the URL http://config.privoxy.org/user-manual/IMAGE.jpg).

Thanks to the whole Privoxy team for making great FOSS software!

## License
General Public License v3, GPL-3  
Copyright © 2001-2021 by Privoxy Developers

## Project status
Actively being matained and updated.
